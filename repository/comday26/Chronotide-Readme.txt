CHRONOTIDE - The Halloween Edition
===================================

Introduction
------------
Since Chronotide is by nature, a scary map. I overhauled the map for the minecraft 1.4 spooky update. (just look at the changelog if you want to see how much I did) :) 
The map is the same old Chronotide but now with uniques mobs, ghosts, halloween decorations, interactions,
creepy music for most areas and bats and witches.
More importantly the ending has been changed LOTS

I hope you enjoy playing the Halloween Edition of the map and remember to leave feedback good or bad as it will help me create better things in the future.

Have Fun
Vladimyr


RULES
-----
Play on easy or normal. (It's been biome editted so there are no random mobs - only the mobs I've let in. so no creepers at all).
Crafting: You may craft with any items you find.
Enchanting and brewing is allowed.
And of course the standard no breaking or placing blocks with the following (sensible) exceptions;
1. When asked/hinted at in a clue/quest note.
2. Should you get stuck in terrain.
3. Vines - You may break them if they start to get in your way but they are useful for exploring the ruined city
4. Beds - If you find a bed you may reposition it to a location of your suiting but do not place it in the nether ;p
5. Torches - Of course you can place/break them ;)


CREDITS:
========

Beta version feedback was supplied by these wonderful people
------------------------------------------------------------

8BitDame: http://www.youtube.com/user/8bitdame
Anistuffs: http://www.youtube.com/user/anistuffs
jGazMom: http://www.youtube.com/user/jgazmom
Nirgalbunny: http://www.youtube.c...ser/Nirgalbunny
Rsmalec: http://www.youtube.com/user/rsmalec
ShawnVMartin: http://www.youtube.c...er/shawnvmartin
Thungon217: http://www.youtube.com/user/Thungon217

Contraptions Used - (credit where credit is due)
-----------------------------------------------

Thungon217 - For the all new improved counter that controls the vaults :D
TexelElf - MCEdit filters (http://www.youtube.com/watch?v=ko6Efrh5KAI&feature=plcp)
Sethbling - MCEdit filters (http://www.youtube.com/watch?v=dMDnbyFDvXQ)
ShagzDesign - For the 3x3 door design (http://www.youtube.com/watch?v=02ja2hop8JU&feature=relmfu)
Davve - For Noteblock Studio (http://www.minecraftforum.net/topic/136749-minecraft-note-block-studio-150000-downloads/)
NBTEDIT - http://www.minecraftforum.net/topic/6661-nbtedit/
MCEDIT - http://www.minecraftforum.net/topic/13807-mcedit-minecraft-world-editor-now-open-source/

And a huge thanks to everyone for supporting me while creating this map.
You have all been brilliant.

changelog
=========

v1.4
General
-------
- Added Darkness interaction (in a lovely indigo colour) throughout all of the map.
- Halloween Music added to most zones including the following themes: twilight zone,Halloween,addams family,candyman, and Funeral march.
- Witches will appear in most zones (it is halloween after all)
- Bats are littering the place - most are naturally spawning.
- .02% less vines
- Each zone will have its own unique ghost.
- All zones contain new uniques mobs (most replace the existing vanilla mobs)
- The end sequence has been drastically overhauled and includes an Arena battle.
Overworld
---------
- Time is regularly being set to midnight
- Weather is regularly set to clear to avoid lag spikes.
- Fixed some places where player can get stuck.
- All nether portals are now pre-activated. This is due to the large number of bats using nether portals making random portals
  appear in the overworld.
- Added more torches (since it's night time now)
- Fixed the clock having locked repeater issues.
- Changed diamond portal to teleport player to arena.

wintertide - Improved fire damage. Rewired tnt
Zentrium - Fixed Blaze dispensor. Fixed Silverfish falling through ceiling
Theatre - Rewired the DIE sign
Bank Street - SVM Band play munsters. Fixed a place where player could get stuck.
University Street - Fixed back windows not being blocked. Removed a golem grinder.
Sewers - Added a smattering of mossy cobble. Added torches in chests. Replaced final battle.

v1.31
- Brand new counter to cycle through the vaults. This new counter will skip vaults if they have
  already been completed. Which speeds up the waiting times for available vaults.
  Thanks go to Thungon217 for creating this new counter.
- The Cemetery has had some signs changed and Crianas diary has been updated.
- The Sewers has been updated to break up the monotony of the walls and to give players landmarks.
  While making those changes I also added dripping water effects and extra atmospheric bits :)

V1.3
Release version

