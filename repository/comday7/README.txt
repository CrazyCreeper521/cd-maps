BattleSheep

This map is designed exclusively for multiplayer. Recommended players is 16-32


HOW TO PLAY:

The map contains two identical BattleSheep with accessory grass block and pasture. Each BattleSheep 
contains all the materials needed for epic battle. The goal is to kill all the sheep located
on the opposing teams pasture. Once all the sheep belonging to one team are killed the game is over.
You can defend your own sheep in any way you can conceive of, but you can not breed more sheep. 

To start a game, divide players into two teams, and set your team's switch to
'READY'. Once both sides are ready the gate will open.

Players may build or craft whatever they can with the resources available.

Players may not build in, on, directly next to, or behind, the spawn platform
or spawn tunnels.

Players may not fight in, or on the spawn platform, or spawn tunnels.

If a player respawns on the spawn platform they must immediately proceed to their sheep.

Players should not harm their own sheep.

Players should use gold or chain helms to identify teams unless other team
identification methods are used.



Recommended server.properties options
----------------------
allow-nether=false
allow-flight=true
spawn-animals=true
spawn-monsters=false
pvp=true
difficulty=3
gamemode=0
max-players=32
----------------------

allow-flight is enabled to prevent people from getting kicked when they fall
into the void.


BattleSheep By DJQJ


Spawn plarform By Dewtroid

Special thanks to Dewtroid for the inspiration to make this map.