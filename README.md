Keep the spreadsheet updated: 
https://docs.google.com/spreadsheets/d/152IHMXZEodW_9gI1f83Ck_3Wybp1adXkxyvdqyihc0U/edit#gid=1196153181

Do not edit anything in the repository folder.

Make sure maps are pruned and have basic PGM-loadable XML before puting them into the wip folder.

Maps in the done folder should be ready to be loaded; they should have map.png, be named in lower-case letters and have no errors or game breaking issues.
